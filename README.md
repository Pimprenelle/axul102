# Axul 102

## La présentation
Dans le cadre de [l'Université Populaire du Pays d'Aix](http://up-aix.com/), l'[AXUL](https://axul.org/) intervient pour une présentation sur le thème de "Promouvoir le logiciel libre".  
Cet extrait de présentation traite de la partie des modéles économiques dans le logiciel libre, et la contribution participative

Les notes de préparation sont disponible en cliquant [ici](note.md)
## AccesSlide
Cette présentation repose sur le framework AccesSlide, écrit en HTML/CSS/JS. Voici les liens pour consulter la documentation en [Version française](READMEFR.md)/[English Version](README_old.md)

## Licence

Tout le code fourni par [Access42](http://www.access42.net) est sous licence GNU GENERAL PUBLIC LICENSE V3.

En ce qui concerne les modifications et les visuels, elles seront sous copyleft et listées ci-dessous.