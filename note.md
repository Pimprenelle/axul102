# Axul 102 : Promouvoir le logiciel libre

## Le modéle économique et contributif du logiciel libre

Le logiciel libre est au coeur de nombreuses installations que ce soit dans dans l'usage personnel/des instistutions/dans le monde de l'éducation et de l'entreprise. Différents acteurs contribuent à différentes solutions libres, on en repérent quelques familles :
* Les bénévoles
* Les donateurs populaires
* Les mécénes (Entreprises, fondations, associations ...)
* Les chercheurs
* Le monde de l'éducation
* Les entreprises

Dans ce qui va suivre, je vais aborder quelques modèles économiques, puis, les différents modes de contribution possibles.

### Les différents modèles économiques

#### Le don:
Modèle poupulaire & utilisé par des grands acteurs (Wikimédia Foudation, LibreOffice, GIMP, Framasoft ...) comme pour des projets plus confidentielles, c'est un modèle simple à mettre en oeuvre, avec la facilité de paiement que l'on peut mettre en place sur le web (ex: Paypal, Liberapay, Cryptomonnaies ...).

#### Le Mécénat :
Quand le soutient provient de fonds privés et/ou des donations par projet (institutions/fondations), c'est le cas des projets suivants :
* Mozilla Firefox
* Ubuntu
* Blender 

#### Prestations de services :
Certaines entreprises livrent leur code sous licence libre, c'est le cas d'acteurs comme Red Hat, WordPress avec la société Automattic, VLC ...

Ces sociétés peuvent vendre du service personnalisé, du support/maintenance, de l'hébergement, de la documentation ...

#### Et bien d'autres ...
Il existe une multitude d'autres sources de financement comme le financement participatif, la publicité, le sponsoring, la certification, la vente de produits dérivées ... 

### Notes pour slides :

#### WordPress

WordPress, célébre système de gestion de contenu est installé sur plus 33% du web. WordPress est porté par la société Américaine Automattic, ainsi qu'une communauté de bénévole. Dans le choix de son modèle économique, Automattic à choisi de fournir le code sous licence GnuGPLv3, la documentation ainsi que des versions packagée de solution, disponible sur [WordPress.org](https://fr.wordpress.org/). Pour son offre payante, Automattic s'est spécialisée dans de l'hébergement freemium & premium de WordPress et de fournir différentes extensions/thèmes bénéficiant de fonctionnalités supplémentaires [WordPress.com](https://wordpress.com/). Son offre est étendu a du service supplémentaire a travers Akismet, Jetpack, WooComerce ....

#### Framasoft

Association Francaise loi 1901, héberge et produits des services libres. Tous sont utilisable gratuitement et répondent aux 4 libertés fondamentales du Logiciel Libre. Le modèle économique de Framasoft, repose sur le mécenat (populaire, institutionel & privée). Aller voir détails sur [Stats Framasoft](https://framasoft.org/fr/association/)

#### Démonstration de LiberaPay

<hr>

### La Contribution
Si le financement est aspect non négligeable du logiciel libre, l'autre aile de la réussite d'un projet libre est la contribution.
Il existe différent moyens de contribution accessible en tant qu'utilisateur, en étant initié ou confirmé dans la programmation, l'administration de système au autre discipline lié à l'informatique.

#### La promotion :
Le plus accessible pour faire gagner un projet en popularité, c'est d'en parler à votre entourage, sur votre blog/réseau social, de le faire tester à vos proches/connaissances/...

#### Le rapport de bogues :

Les logiciels évoluent et les fonctionnalités ne sont pas toujours optimales, selon les spécificités de notre machine et des ses réglages. Il est toujours bienvenue de contacter l'équipe dérriere le projet, pour faire remonter le problème en détaillant un maximum le bogue rencontré.  
-> Astuce : Souvent des modéles de rapport existe sur les sites des projets, n'hésitez pas a suivre le modèle, pour que l'équipe dispose d'un maximum de renseignements pour solutionner.

#### La traduction :
Vous êtes bilingue/trilingue/polyglotte ? La langue par défaut est l'Anglais, hors dans un soucis d'accessibilité, la traduction sur plusieurs langues est plus qu'appréciée et aidera un nombre conséquent de personnes à adopter un projet libre.

#### L'entraide :
Vous avez une bonne connaissance/maîtrise d'un logiciel/solution, n'hésitez pas à rendre service à d'autres personnes que ce soit sur les communautés internet (Forum, Messagerie Instantanée de groupe)

#### La contribution technique :
##### Coder :
Faire progresser un logiciel en aidant l'équipe en codant, proposer une future/amélioration, résoudre un bug ...
##### De la contribution graphique/designer/expérience utilisateur :
Vous êtes avez un profil UX/UI, graphiste ... Contribuer en aidant à l'ergonomie ou à l'image d'une application ou services libres
##### Contribuer à la documentation :
Vous avez une expertise sur une solution, vous pouvez soumettre vos contributions à la documentation pour la rendre encore plus exhaustive et améliorer l'appropriation des actuels et futur utilisateurs

#### A prévoir dans les slides :
Démonstration d'une platefome collaborative comme Github/Gitlab

<hr>

### Conclusion
La diversité des modéles économiques permet aux solutions libres d'être pérenne et de garder une autonomie necessaire pour évoluer librement.S
Contribuer au logiciel libre peut être effectué par toutes & tous, queque soit son niveau technique ou non, ses moyens financiers, ses compétences linguistiques ...


====================================
##### Sources utilisées :
[Modéles économiques liés aux logiciels libres](https://aful.org/media/document/modeles-economiques.pdf)